﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPlayer : MonoBehaviour {

	public GameObject player;

	void FixedUpdate() {
		this.transform.position = new Vector3 (player.transform.position.x, this.transform.position.y, this.transform.position.z);
	}
}