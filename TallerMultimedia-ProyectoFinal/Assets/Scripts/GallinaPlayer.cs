﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GallinaPlayer : MonoBehaviour {

	public Animator gallinaAnim;
	public Rigidbody2D rigid;
	public Transform trans;
	public SpriteRenderer render;
	public bool isMoving;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if(Input.GetKeyDown(KeyCode.Space)){

			rigid.AddForce(Vector2.up * 10, ForceMode2D.Impulse);

			gallinaAnim.SetTrigger("SpaceKeyPressed");

			//apaga parametro que gallina toca plataforma
			gallinaAnim.SetBool("GallinaTouchedPlatform",false);

		}

		isMoving = false;

		// por defecto apagamos este parametro
		//gallinaAnim.SetBool ("GallinaIsMoving", false);

		if (Input.GetKey (KeyCode.RightArrow)) {
			
			//Mover gallina a derecha
			trans.Translate (Vector3.right * 0.1f);

			render.flipX = false;


			//indicamos que la gallina se esta moviendo
			isMoving = true;
			//gallinaAnim.SetBool ("GallinaIsMoving", true);
		}

			if (Input.GetKey (KeyCode.LeftArrow)) {

				//Mover gallina a derecha
				trans.Translate (Vector3.left * 0.1f);

				render.flipX = true;

			//indicamos que la gallina se esta moviendo
			isMoving = true;
			//gallinaAnim.SetBool ("GallinaIsMoving", true);
			}

		if (isMoving == true) {
			gallinaAnim.SetBool ("GallinaIsMoving", true);
		} else {
			gallinaAnim.SetBool ("GallinaIsMoving", false);
		}
}
	void OnCollisionEnter2D(Collision2D collision) {

		// Si la tag del objecto que colisiono con el personaje es la adecuada
		if (collision.gameObject.tag == "Platform") {

		
			//Prende parametro que gallina toca plataforma
			gallinaAnim.SetBool("GallinaTouchedPlatform",true);
		}
	}
}