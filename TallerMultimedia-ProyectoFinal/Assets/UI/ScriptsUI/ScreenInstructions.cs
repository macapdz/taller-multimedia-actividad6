﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenInstructions : MonoBehaviour
{
    public void GoToPlay(int Game)
    {

        SceneManager.LoadScene(Game);
    }
}
